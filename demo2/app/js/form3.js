(function($){
	"use strict";
	$.fn.validate = function() {
		let that    = this;
		let name    = that.find('input[name="name"]');
		let email   = that.find('input[name="email"]');
		let phone   = that.find('input[name="phone"]');
		let message = that.find('textarea[name="message"]');
	
		that.on('submit',function(){
			let check = true;
			if ($(name).val().trim() === '') {
				showValidate(name);
				check = false;
			}
			if ($(email).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) === null) {
				showValidate(email);
				check = false;
			}
			if ($(phone).val().trim().match(/^\+?[0-9]{10,11}$/) === null) {
				showValidate(phone);
				check = false;
			}
			if ($(message).val().trim() === '') {
				showValidate(message);
				check = false;
			}
			return check;

		});
		that.find('.item__input').each(function() {
            $(this).focus(function() {
                hideValidate(this);
            });
        });

		function showValidate(input) {
			let thisAlert = $(input).parent();
			$(thisAlert).addClass('alert-validate');
		}
		function hideValidate(input) {
			let thisAlert = $(input).parent();
			$(thisAlert).removeClass('alert-validate');
		}
		return this;
	};
})(jQuery);

$(document).ready(function() {
    $('.form').validate();
});