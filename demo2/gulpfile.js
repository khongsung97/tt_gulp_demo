var gulp        = require('gulp');
var sass        = require('gulp-sass');
var browserSync = require('browser-sync').create();
var runSequence = require('run-sequence');
var reload      = browserSync.reload;
var useref      = require('gulp-useref');
var uglify      = require('gulp-uglify-es').default;
var cssnano     = require('gulp-cssnano');
var gulpIf      = require('gulp-if');
// var imagemin = require('gulp-tinypng');
var imagemin    = require('gulp-imagemin');
var cache       = require('gulp-cache');
var del         = require('del');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');


// complie *.scss
gulp.task('sass', () => {
	return gulp .src('app/sass/**/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		.pipe(gulp.dest('app/css'))
		   		.pipe(reload({stream:true}));
});

// concat file .js and .css -> dist
gulp.task('useref', () => {
	return gulp .src('app/*.html')
				.pipe(useref())
				.pipe(gulpIf('*.css', cssnano()))
				.pipe(gulpIf('*.js', uglify()))
				.pipe(gulp.dest('./dist'));

});

gulp.task('rename', () => {
	return gulp .src('app/index.html')
				.pipe(gulpIf('*.css', rename('main.min.css')))
				.pipe(gulpIf('*.js', rename('main.min.js')))
				.pipe(gulp.dest('./dist'));
})

// compress *.js
gulp.task('compress:js', () => {
	return gulp .src('app/js/*.js')
				.pipe(sourcemaps.init())
				.pipe(jshint())
				.pipe(jshint.reporter('default'))
				.pipe(uglify())
				.pipe(concat('main.min.js'))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest('./dist/js'));
})

// compress *.css
gulp.task('compress:css', () => {
	return gulp .src('app/css/*.css')
				.pipe(sourcemaps.init())
				.pipe(cssnano())
				.pipe(sourcemaps.write())
				.pipe(gulp.dest('./dist/css'))
})

// minify images
gulp.task('images', () => {
	return gulp .src('app/images/**/*.+(png|jpg|gif|svg)')
				.pipe(imagemin(/*'e6E5XOQesODN6d1XGrabF8F8khmqw7Vw'*/))
				.pipe(gulp.dest('dist/images'));
});

// minify img in foder css
gulp.task('images:css', ()=> {
	return gulp .src('app/css/images/**/*.+(png|jpg|gif|svg)')
				.pipe(imagemin())
				.pipe(gulp.dest('dist/css/images'));
})

// copy fonts to dist folder
gulp.task('fonts', () => {
	return gulp .src('app/fonts/**/*')
				.pipe(gulp.dest('dist/fonts'))
});

// clear dist
gulp.task('clear:dist', () => {
	return del.sync('dist');
})

// clear cache
gulp.task('clear:cache', (callback) => {
	return cache.clearAll(callback);
})

// create server
gulp.task('server', () => {
	browserSync.init({
		server: {
			baseDir: 'app'
		},
		port: 8082
	})
});

// listen event change
gulp.task('watch', () => {
	gulp.watch('app/sass/**/*.scss', ['sass']);
	gulp.watch('app/*.html');
	gulp.watch('app/js/**/*.js');
});

gulp.task('default', (callback) => {
	runSequence('clear:cache', ['sass', 'server'], 'watch', callback);
});

gulp.task('build', (callback) => {
	runSequence('sass', ['useref', 'images', 'images:css', 'fonts'], callback);
});

