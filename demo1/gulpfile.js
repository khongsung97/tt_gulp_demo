var gulp        = require('gulp');
var sass        = require('gulp-sass');
var browserSync = require('browser-sync').create();
var runSequence = require('run-sequence');
var reload      = browserSync.reload;
var useref      = require('gulp-useref');
var uglify      = require('gulp-uglify');
var cssnano     = require('gulp-cssnano');
var gulpIf      = require('gulp-if');
// var imagemin    = require('gulp-tinypng');
var imagemin    = require('gulp-imagemin');
var cache       = require('gulp-cache');
var del         = require('del');

// complie *.scss
gulp.task('sass', () => {
	return gulp .src('app/sass/**/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		.pipe(gulp.dest('app/css'))
		   		.pipe(reload({stream:true}));
});

// concat file .js and .css -> dist
gulp.task('useref', () => {
	return gulp .src('app/*.html')
				.pipe(useref())
				.pipe(gulpIf('*.js', uglify()))
				.pipe(gulpIf('*.css', cssnano()))
				.pipe(gulp.dest('dist'))

});

// minify images
gulp.task('images', () => {
	return gulp .src('app/images/**/*.+(png|jpg|gif|svg)')
				.pipe(imagemin(/*'e6E5XOQesODN6d1XGrabF8F8khmqw7Vw'*/))
				.pipe(gulp.dest('dist/images'));
});

// minify img in foder css
gulp.task('images:css', ()=> {
	return gulp .src('app/css/images/**/*.+(png|jpg|gif|svg)')
				.pipe(imagemin())
				.pipe(gulp.dest('dist/css/images'));
})

// copy fonts to dist folder
gulp.task('fonts', () => {
	return gulp .src('app/fonts/**/*')
				.pipe(gulp.dest('dist/fonts'))
});

// clear dist
gulp.task('clear:dist', () => {
	return del.sync('dist');
})

// clear cache
gulp.task('clear:cache', (callback) => {
	return cache.clearAll(callback);
})

// create server
gulp.task('server', () => {
	browserSync.init({
		server: {
			baseDir: 'app'
		},
	});
});

// listen event change
gulp.task('watch', () => {
	gulp.watch('app/sass/**/*.scss', ['sass']);
	gulp.watch('app/*.html', reload);
	gulp.watch('app/js/**/*.js', reload);
});

gulp.task('default', (callback) => {
	runSequence('clear:cache', ['sass', 'server'], 'watch', callback);
});

gulp.task('build', (callback) => {
	runSequence('sass', ['useref', 'images', 'images:css', 'fonts'], callback);
});

